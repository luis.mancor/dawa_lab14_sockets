import React, {useState, useEffect} from 'react';  //useeffect lifecycle of methods inside hooks
import queryString from 'query-string';  //retrieve data from url
import io from 'socket.io-client';

import './Chat.css';
//import TextContainer from '../TextContainer/TextContainer';
import InfoBar from '../InfoBar/InfoBar';
import Input from '../Input/Input';
import Messages from '../Messages/Messages';

let socket;

const Chat = ({location}) => {

    const [name, setName] = useState('');  
    const [room, setRoom] = useState('');
    const [message, setMessage] = useState('');
    const [messages, setMessages] = useState([]);
    const ENDPOINT = 'localhost:5000';

    useEffect( () => {
        const { name, room } = queryString.parse(location.search);  //data but destructured

        socket = io(ENDPOINT);
        setRoom(room);
        setName(name);
        
        socket.emit('join', {name, room}, () =>{
            //alert(error);
        });
        //console.log(socket)
        //console.log(name,room);
        //console.log(data); //return object
        //console.log(location.search); //only returns parameters
        return () =>{  //component will unmount   NO ESTA EN GIT
            socket.emit('disconnect');
            socket.off();
        }
    },[ENDPOINT, location.search]); //El array indica que se reiniciara el useEffect cuando hayan cambios en endoint y locaton.search

    /*
    useEffect( () => {
        socket.on('message', (message) =>{
            setMessages([...messages, message]);

        });
    },[messages]);
*/
    useEffect(() => {  //Changed
        socket.on('message', message => {
          setMessages(messages => [ ...messages, message ]);
        });
        
        /*
        socket.on("roomData", ({ users }) => {
          setUsers(users);
        });
        */
    }, []);

    //funcion para enviar mensajes
    const sendMessage = (event) => {
        event.preventDefault();
    
        if(message) {
          socket.emit('sendMessage', message, () => setMessage(''));
        }
    }

    console.log(message, messages);

    return(
        <div className="outerContainer">
            <div className="container">
                <InfoBar room={room}/>
                
                <Messages messages={messages}  name={name}/>
                <Input message={message} setMessage={setMessage} sendMessage={sendMessage}/>
                {/*
                    <input value={message} 
                    onChange={ (event) => setMessage(event.target.value)}
                    onKeyPress={event => event.key === 'Enter' ? sendMessage(event) : null}    
                />
                */}

            </div>
        </div>
    )
}

export default Chat;