import React from 'react';
import './Input.css';


const Input = ({message, sendMessage, setMessage}) => (
    

    <form className="form">
        <input 
            className="input"
            type="text"
            placeholder="Ingrese un mensaje..."

            value={message}
            //onChange={({ target: { value } }) => setMessage(value)}
            onChange={ (event) => setMessage(event.target.value)}
            onKeyPress={event => event.key === 'Enter' ? sendMessage(event) : null}     
        />
        <button className="sendButton" onClick={ (event)=>sendMessage(event) } > Enviar </button>

    </form>
)

export default Input;