const express = require('express');
const socketio = require('socket.io');
const http = require('http');
const {addUser, removeUser, getUser, getUsersInRoom} = require('./user'); //user.js
const cors = require('cors');

const PORT = process.env.PORT || 5000

const router = require('./router');

const app = express();
const server = http.createServer(app);
const io = socketio(server);

app.use(router);
app.use(cors());

io.on('connection', (socket) =>{
    //console.log('Tenemos una nueva conexion!');

    socket.on('join', ({name, room}, callback) =>{
        const {error, user} = addUser({id: socket.id, name, room});
        
        if(error){
            return callback(error);
        }

        socket.join(user.room);
        socket.emit('message', {user: 'Admin', text:`${user.name}, bienvenido a la sala ${user.room}`}); //todos
        socket.broadcast.to(user.room).emit('message', {user: 'Admin', text: `${user.name}, se ha unido!`}); //todos menos el

        io.to(user.room).emit('roomData', {room:user.room, users: getUsersInRoom(user.room)});

        //io.to(user.room).emit('roomData', { room: user.room, users: getUsersInRoom(user.room) });
        callback();
        //console.log(name, room);
        //const error = true;
        //if(error){
        //    callback({error: 'error'}); //response inmediately or error handling
        //}
    });

    socket.on('disconnect', () => {
        console.log('Un usuario se retiro.');
        
        const user = removeUser(socket.id);

        if(user) {
        io.to(user.room).emit('message', { user: 'Admin', text: `${user.name} ha salido.` });
        io.to(user.room).emit('roomData', { room: user.room, users: getUsersInRoom(user.room)});
        }
        
    });


    socket.on('sendMessage', (message, callback) => {
        const user = getUser(socket.id);
    
        io.to(user.room).emit('message', { user: user.name, text: message });
    
        callback();
    });
});





server.listen(PORT, () => console.log(`Server inicializado en puerto: ${PORT}`));





// https://github.com/adrianhajdin/project_chat_application
//  https://www.youtube.com/watch?v=ZwFA3YMfkoc